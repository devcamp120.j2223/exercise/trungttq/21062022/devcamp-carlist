import {gCarsObject} from "./info"



function App() {
  return (
    <div>
      <ul>
        {
          gCarsObject.map((element, index) => {
            return <li key={index}>{element.make}  {element.vID}</li>
          })
        }
      </ul>

      <ul>
        { gCarsObject.map((element, index) => {
            return <li>{(element.year >= 2018)?  element.make + " " + element.vID +  " mới " : element.make + " " + element.vID +  " cũ "}</li>
          })}
      </ul>
    </div>
  );
}

export default App;
